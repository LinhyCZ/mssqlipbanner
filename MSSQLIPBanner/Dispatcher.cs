﻿using System.Collections;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text.RegularExpressions;

namespace MSSQLIPBanner
{
    class Dispatcher
    {
        private static Dispatcher dispatcher;
        static Hashtable table = new Hashtable();

        private Logger logger;

        private Dispatcher()
        {
            logger = Logger.GetLogger();
            logger.LogMessage(LogLevel.DEBUG, "New instance of dispatcher.");
        }

        public static Dispatcher GetDispatcher()
        {
            if (dispatcher == null)
            {
                dispatcher = new Dispatcher();
            }

            return dispatcher;
        }

        public void startListening()
        {
            EventLogQuery securityQuery = new EventLogQuery("Security", PathType.LogName);
            EventLogWatcher securityWatcher = new EventLogWatcher(securityQuery);
            securityWatcher.EventRecordWritten += new System.EventHandler<EventRecordWrittenEventArgs>(LogEventFunc);
            securityWatcher.Enabled = true;
            logger.LogMessage(LogLevel.DEBUG, "Started listening to Security events");

            EventLogQuery applicationQuery = new EventLogQuery("Application", PathType.LogName);
            EventLogWatcher applicationWatcher = new EventLogWatcher(applicationQuery);
            applicationWatcher.EventRecordWritten += new System.EventHandler<EventRecordWrittenEventArgs>(LogEventFunc);
            applicationWatcher.Enabled = true;
            logger.LogMessage(LogLevel.DEBUG, "Started listening to Application events");
        }

        public void LogEventFunc(object source, EventRecordWrittenEventArgs e)
        {
            logger.LogMessage(LogLevel.DEBUG, "Entry ID: " + e.EventRecord.Id);
            if (e.EventRecord.Id.ToString().Equals(Config._MSSQL_EVENT_ID()))
            {
                string IPAddress = Parser.ParseMSSQL(e.EventRecord.FormatDescription());
                ProccessIPAddress(ServiceType.MSSQL, IPAddress);
            }

            if (e.EventRecord.Id.ToString().Equals(Config._SECURITY_AUDIT_EVENT_ID()))
            {
                string IPAddress = Parser.ParseAudit(e.EventRecord.FormatDescription());
                ProccessIPAddress(ServiceType.AUDIT, IPAddress);
            }
        }

        public void ProccessIPAddress(ServiceType type, string IPAddress)
        {
            if (IPAddress != "-") {
                if (Config._WHITELISTED_IPS().Any(s => Regex.IsMatch(IPAddress, s)))
                {
                    logger.LogMessage(LogLevel.INFO, "Failed attempt from whitelisted IP:  " + IPAddress);
                }
                else
                {
                    int attemptCount = AddIPTries(IPAddress);

                    logger.LogMessage(LogLevel.INFO, "Attempt count for IP " + IPAddress + ": " + attemptCount);


                    if (attemptCount == Config._BLOCK_LIMIT() || attemptCount % 30 == 0)
                    {
                        SSHSender.GetSSHSender().BlockClient(type, IPAddress, attemptCount);
                    }

                    table[IPAddress] = attemptCount;
                    //eventIDs[index] = true;
                }
            }
        }

        private int AddIPTries(string IPAddress)
        {
            int attemptCount = 1;

            if (table.ContainsKey(IPAddress))
            {
                attemptCount = ((int)table[IPAddress]) + 1;
            }

            return attemptCount;
        }
    }
}
