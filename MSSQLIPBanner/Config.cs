﻿using Newtonsoft.Json;

namespace MSSQLIPBanner
{
    public class Config
    {
        private static Config instance;

        public static void setConfig(Config config)
        {
            instance = config;
        }

        public static string _SSHLogin() { return instance.SSHLogin; }
        public static string _SSHPassword() { return instance.SSHPassword; }
        public static int _SSHPort() { return instance.SSHPort; }
        public static string _SSHIP() { return instance.SSHIP; }

        public static LogLevel _logLevel() { return instance.logLevel; }

        public static string _MSSQL_EVENT_ID() { return instance.MSSQL_EVENT_ID; }
        public static string _SECURITY_AUDIT_EVENT_ID() { return instance.SECURITY_AUDIT_EVENT_ID; }

        public static int _BLOCK_LIMIT() { return instance.BLOCK_LIMIT; }
        public static string _FILE_PATH() { return instance.FILE_PATH; }

        public static string[] _WHITELISTED_IPS() { return instance.WHITELISTED_IPS; }


        [JsonProperty]
        public string SSHLogin;
        [JsonProperty]
        private string SSHPassword;
        [JsonProperty]
        private int SSHPort;
        [JsonProperty]
        private string SSHIP;


        [JsonProperty]
        private LogLevel logLevel;


        [JsonProperty]
        private string MSSQL_EVENT_ID;
        [JsonProperty]
        private string SECURITY_AUDIT_EVENT_ID;


        [JsonProperty]
        private int BLOCK_LIMIT;
        [JsonProperty]
        private string FILE_PATH;


        [JsonProperty]
        private string[] WHITELISTED_IPS;
    }
}
