﻿namespace MSSQLIPBanner
{
    class Parser
    {
        public static string ParseMSSQL(string logMessage)
        {
            string IPAddress = logMessage.Split(':')[2];
            IPAddress = IPAddress.Substring(1, IPAddress.Length - 2);
            return IPAddress;
        }

        public static string ParseAudit(string logMessage)
        {
            string[] IPAddressSplit = logMessage.Split(':');
            string IPAddress = IPAddressSplit[20];
            IPAddress = IPAddress.Split('\n')[0];
            IPAddress = IPAddress.Trim();
            return IPAddress;
        }
    }
}
