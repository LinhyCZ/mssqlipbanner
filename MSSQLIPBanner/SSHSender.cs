﻿using Renci.SshNet;
using System;

namespace MSSQLIPBanner
{
    class SSHSender
    {
        private static SSHSender instance;
        private SshClient client;

        private SSHSender()
        {
            client = new SshClient(Config._SSHIP(), Config._SSHPort(), Config._SSHLogin(), Config._SSHPassword());
            Logger.GetLogger().LogMessage(LogLevel.DEBUG, "Starting SSH client.");

            client.Connect();
            Logger.GetLogger().LogMessage(LogLevel.DEBUG, "Connected SSH client.");
        }

        public static SSHSender GetSSHSender()
        {
            if (instance == null)
            {
                instance = new SSHSender();
            }

            return instance;
        }

        public void BlockClient(ServiceType type, String IPAddress, int attemptCount)
        {
            Logger.GetLogger().LogMessage(LogLevel.INFO, "!!! " + type.ToString() + " Blocking IP: " + IPAddress + " after " + attemptCount + " attempts");

            if (!client.IsConnected)
            {
                Logger.GetLogger().LogMessage(LogLevel.DEBUG, "Connection lost, reconnecting.");
                client.Connect();
            }

            string command = "";
            if (type == ServiceType.AUDIT)
            {
                command = "/ip firewall address-list add address=" + IPAddress + " comment=\"Blocked on " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + "\" list=AuditAutomatedBlocking";
            }
            else
            {
                command = "/ip firewall address-list add address=" + IPAddress + " comment=\"Blocked on " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + "\" list=MSSQLAutomatedBlocking";
            }

            client.RunCommand(command);
            Logger.GetLogger().LogMessage(LogLevel.DEBUG, "Command sent.");
        }

        public void Disconnect()
        {
            client.Disconnect();
            Logger.GetLogger().LogMessage(LogLevel.DEBUG, "Disconnected SSH client.");
        }
    }
}
