﻿using System;
using System.IO;

namespace MSSQLIPBanner
{

    class Logger
    {
        private static Logger logger;

        private TextWriter tw;

        private Logger()
        {
            tw = new StreamWriter(Config._FILE_PATH());
            this.LogMessage(LogLevel.DEBUG, "Logger started.");
        }

        public static Logger GetLogger()
        {
            if (logger == null)
            {
                logger = new Logger();
            }

            return logger;
        }

        public void LogMessage(LogLevel level, string message)
        {
            if (level >= Config._logLevel())
            {
                string formattedMessage = "[" + level.ToString() + "] " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + ": " + message;
                Console.WriteLine(formattedMessage);
                tw.WriteLine(formattedMessage);
            }
        }

        public void Close()
        {
            tw.Close();
            tw.Dispose();
        }
    }
}
