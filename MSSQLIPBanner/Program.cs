﻿using Newtonsoft.Json;
using System.IO;
using System.Threading;

namespace MSSQLIPBanner
{
    class Program
    {
        static AutoResetEvent signal = new AutoResetEvent(false);
        static string CONFIG_PATH = "Config.json";

        public static void Main()
        {
            Config loadedConfig = loadConfig();
            Config.setConfig(loadedConfig);
            Dispatcher dispatcher = Dispatcher.GetDispatcher();
            dispatcher.startListening();

            signal.WaitOne();
        }

        public static Config loadConfig()
        {
            Config parsedConfig = JsonConvert.DeserializeObject<Config>(File.ReadAllText(CONFIG_PATH));
            return parsedConfig;
        }
    }
}
